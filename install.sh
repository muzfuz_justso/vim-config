#!/bin/bash

rm -rf ~/.vim
rm -f ~/.vimrc

cp -rf .vim ~/
cp -f .vimrc ~/
